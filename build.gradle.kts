@file:Suppress("UnstableApiUsage")

buildscript {

    repositories {
        mavenCentral()
        google()
    }
}

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.application) apply false
    alias(libs.plugins.hilt) apply false
    alias(libs.plugins.kotlin.gradlePlugin) apply false
    alias(libs.plugins.kotlin.serialization) apply false
    alias(libs.plugins.kotlin.ksp) apply false
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

plugins {
  id ("org.sonarqube") version "4.2.1.3168"
}

sonar {
  properties {
    property("sonar.projectKey", "vedanshgaur_Seal_AYqygImPztaylSDiycU1")
    property("sonar.projectName", "Seal")
    property("sonar.qualitygate.wait", true)
  }
}

